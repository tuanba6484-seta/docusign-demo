﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Tabs.Master" AutoEventWireup="true"
    CodeBehind="SendDocument.aspx.cs" Inherits="DocuSignSample.SendDocument" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/jquery.ui.all.css" />
    <link rel="stylesheet" type="text/css" href="css/SendDocument.css" />
    <!--<link rel="stylesheet" type="text/css" href="css/SendTemplate.css" />)-->
    <script type="text/javascript" src="js/jquery-1.4.4.js"></script>
    <script type="text/javascript" src="js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="js/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="js/jquery.ui.dialog.js"></script>
    <script type="text/javascript" src="js/jquery.bgiframe-2.1.2.js"></script>
    <script type="text/javascript" src="js/jquery.ui.mouse.js"></script>
    <script type="text/javascript" src="js/jquery.ui.draggable.js"></script>
    <script type="text/javascript" src="js/jquery.ui.position.js"></script>
    <script type="text/javascript" src="js/Utils.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            activate();
            var today = new Date().getDate();
            $("#reminders").datepicker({
                showOn: "button",
                buttonImage: "images/calendar.png",
                buttonImageOnly: true,
                minDate: today
            });
            $("#expiration").datepicker({
                showOn: "button",
                buttonImage: "images/calendar.png",
                buttonImageOnly: true,
                minDate: today + 3
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="SendDocumentForm" action="SendDocument.aspx" runat="server">
    <p>
    </p>
    <table id="recipientList" name="recipientList" class="recipientList">
        <tr class="recipientListHeader">
            <th>
                Fuill Name
            </th>
            <th>
                Bỉthday
            </th>
            <th>
                SSN
            </th>
            <th>
                E-mail
            </th>
        </tr>
        <tr id="Recipient1" name="Recipient1">
            <td>
                <input id="RecipientName" type="text" name="RecipientName" />
            </td>
            <td>
                <input id="RecipientBirthday" type="text" name="RecipientBirthday" />
            </td>
            <td>
                <input id="RecipientSSN" type="text" name="RecipientSSN" />
            </td>
            <td>
                <input id="RecipientEmail" type="email" name="RecipientEmail" />
            </td>
        </tr>
    </table>
    <p />
    <table class="submit">
        <tr>
            <td class="fourcolumn">
            </td>
            <td class="fourcolumn">
                <input type="submit" value="Signing" name="SendNow" style="width: 100%;" class="docusignbutton orange" />
            </td>
            <td class="fourcolumn">
            </td>
        </tr>
    </table>
    </form>
</asp:Content>
