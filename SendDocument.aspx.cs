﻿/**
 * @copyright Copyright (C) DocuSign, Inc.  All rights reserved.
 *
 * This source code is intended only as a supplement to DocuSign SDK
 * and/or on-line documentation.
 * 
 * This sample is designed to demonstrate DocuSign features and is not intended
 * for production use. Code and policy for a production application must be
 * developed to meet the specific data and security requirements of the
 * application.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Web;
using DocuSignSample.DocuSignAPI;
using DocuSignSample.resources;

namespace DocuSignSample
{
    public partial class SendDocument : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!LoggedIn())
            {
                Response.Redirect("LogIn.aspx");
            }
                if (Page.IsPostBack && Request.Form["__EVENTTARGET"] != logoutCtrlName)
                {
                    // Create the recipient
                    Recipient recipient = new Recipient();
                    recipient.Email = Request.Form[Keys.RecipientEmail];
                    recipient.UserName = Request.Form[Keys.RecipientName];
                    recipient.Type = RecipientTypeCode.Signer;
                    recipient.ID = "1";
                    recipient.CaptiveInfo = new RecipientCaptiveInfo();
                    recipient.CaptiveInfo.ClientUserId = "1000";
                    recipient.RoutingOrder = 1;

                    // Create the envelope content
                    Envelope envelope = new Envelope();
                    envelope.Subject = "Docusign Demo Subject";
                    envelope.EmailBlurb = "Docusign Demo EmailBlurd";
                    envelope.Recipients = new Recipient[] { recipient };
                    envelope.AccountId = Session[Keys.ApiAccountId].ToString();

                    // Attach the document(s)
                    envelope.Documents = GetDocuments();
                    envelope.Tabs = new Tab[5];

                    // Create a new signature tab
                    Tab tab = new Tab();
                    tab.DocumentID = "1";
                    tab.RecipientID = "1";
                    tab.Type = TabTypeCode.SignHere;
                    tab.PageNumber = "1";
                    tab.XPosition = "500";
                    tab.YPosition = "700";
                    envelope.Tabs[0] = tab;

                    // Create a new FullName tab
                    Tab tabText = new Tab();
                    tabText.DocumentID = "1";
                    tabText.RecipientID = "1";
                    tabText.FontSize = FontSize.Size20;
                    tabText.FontSizeSpecified = true;
                    tabText.Type = TabTypeCode.FullName;
                    tabText.TabLabel = Request.Form[Keys.RecipientName].ToString();
                    tabText.PageNumber = "1";
                    tabText.XPosition = "200";
                    tabText.YPosition = "450";
                    envelope.Tabs[1] = tabText;

                    // Create a new Birthday tab
                    Tab tabBirthday = new Tab();
                    tabBirthday.DocumentID = "1";
                    tabBirthday.RecipientID = "1";
                    tabBirthday.Type = TabTypeCode.Custom;
                    tabBirthday.CustomTabType = CustomTabType.Text;
                    tabBirthday.CustomTabTypeSpecified = true;
                    tabBirthday.CustomTabLocked = true;
                    tabBirthday.PageNumber = "1";
                    tabBirthday.XPosition = "200";
                    tabBirthday.YPosition = "525";
                    tabBirthday.Value = Request.Form[Keys.RecipientBirthday].ToString();
                    envelope.Tabs[2] = tabBirthday;

                    // Create a new SSN tab
                    Tab tabSSN = new Tab();
                    tabSSN.DocumentID = "1";
                    tabSSN.RecipientID = "1";
                    tabSSN.Type = TabTypeCode.Custom;
                    tabSSN.CustomTabType = CustomTabType.Text;
                    tabSSN.CustomTabTypeSpecified = true;
                    tabSSN.CustomTabLocked = true;
                    tabSSN.PageNumber = "1";
                    tabSSN.XPosition = "475";
                    tabSSN.YPosition = "525";
                    tabSSN.Value = Request.Form[Keys.RecipientSSN].ToString();
                    envelope.Tabs[3] = tabSSN;

                    // Create a new Date tab
                    Tab tabDate = new Tab();
                    tabDate.DocumentID = "1";
                    tabDate.RecipientID = "1";
                    tabDate.Type = TabTypeCode.Custom;
                    tabDate.CustomTabType = CustomTabType.Text;
                    tabDate.CustomTabTypeSpecified = true;
                    tabDate.CustomTabLocked = true;
                    tabDate.PageNumber = "1";
                    tabDate.XPosition = "475";
                    tabDate.YPosition = "485";
                    tabDate.Value = DateTime.Now.ToShortDateString();
                    envelope.Tabs[4] = tabDate;

                    if (null != Request.Form[Keys.SendNow])
                    {
                        //we want to send the form ASAP
                        //SendNow(envelope);
                        //edit before sending -- embedded sending
                        EmbedSending(envelope);
                    }

                    else
                    {
                        //edit before sending -- embedded sending
                        EmbedSending(envelope);
                    }
                }
            
        }

        protected void SendNow(Envelope envelope)
        {
            APIServiceSoapClient client = CreateAPIProxy();
            try
            {
                // Create and send the envelope in one step
                EnvelopeStatus status = client.CreateAndSendEnvelope(envelope);

                // If we succeeded, go to the status
                if (status.SentSpecified)
                {
                    AddEnvelopeID(status.EnvelopeID);
                    Response.Redirect("GetStatusAndDocs.aspx", false);
                }

            }
            catch (Exception ex)
            {
                GoToErrorPage(ex.Message);
            }
        }

        protected void EmbedSending(Envelope envelope)
        {
            APIServiceSoapClient client = CreateAPIProxy();
            try
            {
                // Create the envelope (but don't send it!)
                EnvelopeStatus status = client.CreateAndSendEnvelope(envelope);
                // If it created successfully, redirect to the embedded host
                if (status.Status == EnvelopeStatusCode.Sent)
                {
                    string navURL = String.Format("{0}?envelopeID={1}&accountID={2}&source=Document", "SigningDocument.aspx", status.EnvelopeID,
                        envelope.AccountId);
                    Response.Redirect(navURL, false);
                }
            }
            catch (Exception ex)
            {
                base.GoToErrorPage(ex.Message);
            }
        }

        private Document[] GetDocuments()
        {
            var runningList = new List<Document>();

            // Use the document that came with this sample
            var stockDocument = new Document
                {
                    PDFBytes = Resources.Document_Demo,
                    Name = "Demo Document",
                    ID = "1",
                    FileExtension = "pdf"
                };

            Debug.Assert(null != stockDocument.PDFBytes);
            runningList.Add(stockDocument);

            
            return runningList.ToArray();
        }

        private void AddCustomTextTab(string pageThree, List<Tab> runningList)
        {
            //Custom text tab
            var favColor = new Tab
                {
                    Type = TabTypeCode.Custom,
                    CustomTabType = CustomTabType.Text,
                    CustomTabTypeSpecified = true,
                    DocumentID = "1",
                    PageNumber = pageThree,
                    RecipientID = "1",
                    XPosition = "301",
                    YPosition = "416"
                };

            if (null != Request.Form[Keys.CollabFields])
            {
                favColor.SharedTab = true;
                favColor.SharedTabSpecified = true;
                favColor.RequireInitialOnSharedTabChange = true;
                favColor.RequireInitialOnSharedTabChangeSpecified = true;
            }

            runningList.Add(favColor);
        }
    }
}
