﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocuSignSample.resources;
using System.IO;

namespace DocuSignSample
{
    public partial class SigningDocument : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!LoggedIn())
            {
                Response.Redirect("LogIn.aspx");
            }
            if (Request.Form["__EVENTTARGET"] != logoutCtrlName)
            {
                DocuSignAPI.APIServiceSoapClient client = CreateAPIProxy();
                string token = null;

                // Get the information we need from the query string
                string accountID = Request[Keys.AccountId];

                // Get the information we need from the query string
                string envelopeID = Request[Keys.EnvelopeId];
                string clientUserId = "1000";

                DocuSignAPI.Envelope envelope = client.RequestEnvelope(envelopeID, true);

                // Construct the recipient token authentication assertion
                // Specify ID, start time, method and domain
                DocuSignAPI.RequestRecipientTokenAuthenticationAssertion assertion = new DocuSignAPI.RequestRecipientTokenAuthenticationAssertion();
                assertion.AssertionID = "42581612-4fef-4482-aacc-076319ca5c0f";
                assertion.AuthenticationInstant = DateTime.Now;
                assertion.AuthenticationMethod = DocuSignAPI.RequestRecipientTokenAuthenticationAssertionAuthenticationMethod.None;
                assertion.SecurityDomain = "localhost";

                // Construct the URLs based on username
                DocuSignAPI.Recipient recipient = envelope.Recipients[0];
                DocuSignAPI.RequestRecipientTokenClientURLs urls = new DocuSignAPI.RequestRecipientTokenClientURLs();
                String urlBase = Request.ApplicationPath + "SendDocument.aspx";// @"/SendDocument.aspx";
                urls.OnSigningComplete = urlBase + "?event=SignComplete&uname=" + recipient.UserName;
                urls.OnViewingComplete = urlBase + "?event=ViewComplete&uname=" + recipient.UserName;
                urls.OnCancel = urlBase + "?event=Cancel&uname=" + recipient.UserName;
                urls.OnDecline = urlBase + "?event=Decline&uname=" + recipient.UserName;
                urls.OnSessionTimeout = urlBase + "?event=Timeout&uname=" + recipient.UserName;
                urls.OnTTLExpired = urlBase + "?event=TTLExpired&uname=" + recipient.UserName;
                urls.OnIdCheckFailed = urlBase + "?event=IDCheck&uname=" + recipient.UserName;
                urls.OnAccessCodeFailed = urlBase + "?event=AccessCode&uname=" + recipient.UserName;
                urls.OnException = urlBase + "?event=Exception&uname=" + recipient.UserName;
                

                // Request the token to edit the envelope
                try
                {
                    //string retURL = Request.Url.AbsoluteUri.Replace("EmbeddedHost.aspx", "pop.html?source=document");
                    token = client.RequestRecipientToken(envelopeID, clientUserId, recipient.UserName, recipient.Email, assertion, urls);
                    //token = client.RequestSenderToken(envelopeID, accountID, retURL);
                }
                catch (Exception ex)
                {
                    GoToErrorPage(ex.Message);
                }
                
                // Set the source of the iframe to point to DocuSign
                sendingFrame.Attributes[Keys.Source] = token;
            }
        }
        /*
        public void RequestDocumentPDF(object sender, EventArgs e)
        {
            DocuSignAPI.APIServiceSoapClient client = CreateAPIProxy();
            // Get the information we need from the query string
            string accountID = Request[Keys.AccountId];

            // Get the information we need from the query string
            string envelopeID = Request[Keys.EnvelopeId];
            try
            {
                DocuSignAPI.DocumentPDFs documentPDF = client.RequestDocumentPDFs(envelopeID);
                File.WriteAllBytes(@"D:\testpdf.pdf", documentPDF.DocumentPDF[0].PDFBytes);
            }
            catch (Exception ex)
            {
                GoToErrorPage(ex.Message);
            }
        }*/

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            DocuSignAPI.APIServiceSoapClient client = CreateAPIProxy();
            // Get the information we need from the query string
            string accountID = Request[Keys.AccountId];

            // Get the information we need from the query string
            string envelopeID = Request[Keys.EnvelopeId];
            try
            {
                DocuSignAPI.DocumentPDFs documentPDF = client.RequestDocumentPDFs(envelopeID);
                //File.WriteAllBytes(@"D:\testpdf.pdf", documentPDF.DocumentPDF[0].PDFBytes);
                string path = @"D:\" + envelopeID + ".pdf";
                File.WriteAllBytes(path, documentPDF.DocumentPDF[0].PDFBytes);
            }
            catch (Exception ex)
            {
                GoToErrorPage(ex.Message);
            }
        }
    }
}